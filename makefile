.SUFFIXES :
.SUFFIXES : .o .c .f90

.f90.o:
	$(F90) $(F90FLAGS) -c $<

F90=gfortran -cpp -O0 -g -fbounds-check -frange-check -finit-integer=987654321 -finit-real=nan -finit-logical=true -finit-character=64 -D__XML_STANDALONE  -fsanitize=address
#F90=ifort -C -g -traceback -D__XML_STANDALONE
#F90=nvfortran -g -C -Ktrap=fp -Mcache_align -Mpreprocess -Mlarge_arrays -D__XML_STANDALONE

#all: clean testxml.x testdom.x read_psml.x xmltest.x 
all: testleak.x

testleak.x: testleak.o
	$(F90) -o testleak.x testleak.o wxml.o xmltools.o dom.o
testleak.o: dom.o wxml.o

xmltest.x: xmltest.o 
	$(F90) -o xmltest.x xmltest.o xmltools.o pseudo_types.o
xmltest.o: pseudo_types.o xmltools.o xmltest.o 

testdom.x: testdom.o
	$(F90) -o testdom.x testdom.o dom.o
testdom.o: dom.o

testxml.x: testxml.o
	$(F90) -o testxml.x testxml.o wxml.o xmltools.o dom.o
testxml.o: dom.o wxml.o

wxml.o: xmltools.o

clean:
	-/bin/rm *.o *.x *.mod
