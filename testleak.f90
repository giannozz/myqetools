!
program testleak
  !
  use dom
  use wxml
  implicit none
  ! wxml
  type(xmlf_t) :: xf
  ! dom
  type(node), pointer :: root_p
  type(node), pointer :: n1,n2,n3
  type(nodelist), pointer :: l1, l2, l3, l4
  !
  character(len=80) :: xmlfile
  !
  !write(6,'("file name > ")',advance='no')
  !read (5,'(A)') xmlfile
  xmlfile='data-file-schema.xml'
  !xmlfile='file2.xml'
  root_p => parsefile(xmlfile)
  !l1 => getelementsbytagname(root_p,'input')
  !n1 => item(l1,0)
  !l2 => getelementsbytagname(n1,'atomic_structure')
  !n2 => item(l2,0)
  !l3 => getelementsbytagname(n2,'atomic_positions')
  !n3 => item(l3,0)
  !l4 => getelementsbytagname(n3,'atom')
  call destroy ( root_p, 6 )
  !call destroy ( root_p )
  !print *, getlength(l1), getlength(l2), getlength(l3), getlength(l4)
  !
end program testleak
