!
! Copyright (C) 2001-2021 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
program test_simpson
  !
  implicit none
  integer, parameter :: dp = selected_real_kind(14,200)
  real(dp),parameter :: pi = 3.14159265358979323846_dp
  real(dp) :: zmesh, rmax, xmin, dx, x
  real(dp) :: q, qmin, qmax, dq, alpha
  real(dp) :: s1, s2, exact, err1, rel1, err2, rel2
  real(dp), allocatable :: r(:), rab(:), f(:), jl(:)
  integer :: mesh, nq, iq, i, l 
  !
  zmesh= 14.0_dp
  rmax = 100.0_dp
  xmin = -8.0_dp
  dx   =  0.0625_dp
  !
  mesh = (log(zmesh*rmax) - xmin)/dx
  ! mesh = 2*(mesh/2)+1
  !
  allocate (r(mesh), rab(mesh), f(mesh), jl(mesh) )
  do i=1,mesh
     x = xmin + dx * (i-1)
     r(i)  = exp(x)/zmesh
     rab(i)= r(i)*dx
  end do
  write(*,'(/"radial grid information:")')
  write(*,'("dx =",f10.6,", xmin =",f10.6,", zmesh =",f10.6)') &
       dx,xmin,zmesh
  write(*,'("mesh =",i6,", r(1) =",f10.6,", r(mesh) =",f10.6)') &
       mesh, r(1), r(mesh)
  write(*,*) 
  !
  qmin=0_dp
  qmax=8_dp
  dq  =0.1_dp
  alpha=2.0
  nq = (qmax-qmin)/dq+1
  !
  l = 0
  !
  write(*,'("Function to be integrated: alpha =",f10.6)') alpha
  write(*,'("Values at r=0 and at r=rmax:",2f12.8)') &
       exp(-alpha*r(1)**2), exp(-alpha*r(mesh)**2)
  write(*,'("Q-grid: ",i4," points from 0 to ",f7.4," (dq=",f10.4,")")') &
       nq, qmax,dq
  !
  err1 = 0.0_dp
  err2 = 0.0_dp
  rel1 = 0.0_dp
  rel2 = 0.0_dp
  do iq=1,nq
     q = qmin + (iq-1)*dq
     exact = (pi/alpha)**(1.5_dp)*exp(-q**2/4.0_dp/alpha)
     call sph_bes (mesh, r, q, l, jl)
     f(:)  = r(:)**2 * exp(-alpha*r(:)**2) * jl(:)
     ! do i=1,mesh
     !   write(12,'("f(",f7.4,")=",f12.8)') r(i), f(i)
     ! end do
     call simpson      (mesh, f, rab, s1)
     s1 = 4.0_dp*pi*s1
     call simpson_cp90 (mesh, f, rab, s2)
     s2 = 4.0_dp*pi*s2
     err1 = max( err1, abs(s1-exact) )
     rel1 = max( rel1, abs(s1-exact)/abs(exact) )
     err2 = max( err2, abs(s2-exact) )
     rel2 = max( rel2, abs(s2-exact)/abs(exact) )
     ! write(*,'("f(",f7.4,")=",f12.8,"   delta: ",2e12.4)') q,exact,err1,err2
     write(*,'("f(",f7.4,")=",3f12.8)') q,exact,s1,s2
  end do
  write(*,*) 
  write(*,'("simpson     : max abs, rel error = ",2e12.4)') err1,rel1
  write(*,'("simpson_cp90: max abs, rel error = ",2e12.4)') err2,rel2
  
end program test_simpson
!
!-----------------------------------------------------------------------
SUBROUTINE simpson(mesh, func, rab, asum)
  !-----------------------------------------------------------------------
  !
  !     simpson's rule integration. On input:
  !       mesh = the number of grid points (should be odd)
  !       func(i)= function to be integrated
  !       rab(i) = r(i) * dr(i)/di * di
  !     For the logarithmic grid not including r=0 :
  !       r(i) = r_0*exp((i-1)*dx) ==> rab(i)=r(i)*dx
  !     For the logarithmic grid including r=0 :
  !       r(i) = a(exp((i-1)*dx)-1) ==> rab(i)=(r(i)+a)*dx
  !     Output in asum = \sum_i c_i f(i)*rab(i) = \int_0^\infty f(r) dr
  !     where c_i are alternativaly 2/3, 4/3 except c_1 = c_mesh = 1/3
  !
  IMPLICIT NONE
  integer, parameter :: dp = selected_real_kind(14,200)
  INTEGER, INTENT(in) :: mesh
  real(DP), INTENT(in) :: rab (mesh), func (mesh)
  real(DP), INTENT(out):: asum
  !
  real(DP) :: f1, f2, f3, r12
  INTEGER :: i
  !
  asum = 0.0d0
  r12 = 1.0d0 / 3.0d0
  f3 = func (1) * rab (1) * r12

  DO i = 2, mesh - 1, 2
     f1 = f3
     f2 = func (i) * rab (i) * r12
     f3 = func (i + 1) * rab (i + 1) * r12
     asum = asum + f1 + 4.0d0 * f2 + f3
     ! write(10,'("sum: ",f12.8,", func: ",f12.8)') asum, func(i)
     ! write(10,'("sum: ",f12.8,", func: ",f12.8)') asum, func(i+1)
  ENDDO
  !
  ! if mesh is not odd, use open formula instead:
  ! ... 2/3*f(n-5) + 4/3*f(n-4) + 13/12*f(n-3) + 0*f(n-2) + 27/12*f(n-1)
  !!! Under testing
  !
  !IF ( MOD(mesh,2) == 0 ) THEN
  !   print *, 'mesh even: correction:', f1*5.d0/4.d0-4.d0*f2+23.d0*f3/4.d0, &
  !                                      func(mesh)*rab(mesh), asum
  !   asum = asum + f1*5.d0/4.d0 - 4.d0*f2 + 23.d0*f3/4.d0
  !END IF

  RETURN
END SUBROUTINE simpson

!=-----------------------------------------------------------------------
SUBROUTINE simpson_cp90( mesh, func, rab, asum )
  !-----------------------------------------------------------------------
  !
  !    This routine computes the integral of a function defined on a
  !    logaritmic mesh, by using the open simpson formula given on
  !    pag. 109 of Numerical Recipes. In principle it is used to
  !    perform integrals from zero to infinity. The first point of
  !    the function should be the closest to zero but not the value
  !    in zero. The formula used here automatically includes the
  !    contribution from the zero point and no correction is required.
  !
  !    Input as "simpson". At least 8 integrating points are required.
  !
  !    last revised 12 May 1995 by Andrea Dal Corso
  !
  IMPLICIT NONE
  integer, parameter :: dp = selected_real_kind(14,200)
  INTEGER, INTENT(in) :: mesh
  real(DP), INTENT(in) :: rab (mesh), func (mesh)
  real(DP), INTENT(out):: asum
  !
  real(DP) :: c(4)
  INTEGER ::i
  !
  IF ( mesh < 8 ) print *, 'simpson_cp90: too few mesh points'

  c(1) = 109.0d0 / 48.d0
  c(2) = -5.d0 / 48.d0
  c(3) = 63.d0 / 48.d0
  c(4) = 49.d0 / 48.d0

  asum = ( func(1)*rab(1) + func(mesh  )*rab(mesh  ) )*c(1) &
       + ( func(2)*rab(2) + func(mesh-1)*rab(mesh-1) )*c(2) &
       + ( func(3)*rab(3) + func(mesh-2)*rab(mesh-2) )*c(3) &
       + ( func(4)*rab(4) + func(mesh-3)*rab(mesh-3) )*c(4)
  DO i=5,mesh-4
     ! write(11,'("sum: ",f12.8,", func: ",f12.8)') asum, func(i)
     asum = asum + func(i)*rab(i)
  ENDDO

  RETURN
END SUBROUTINE simpson_cp90
!
!--------------------------------------------------------------------
subroutine sph_bes (msh, r, q, l, jl)
  !--------------------------------------------------------------------
  !! Spherical Bessel function.
  !
  !
  implicit none
  integer, parameter :: dp = selected_real_kind(14,200)
  real(dp) :: eps14 = 1.0d-14
  !
  integer :: msh
  !! number of grid points points
  integer :: l
  !! angular momentum (-1 <= l <= 6)
  real(DP) :: r (msh)
  !! radial grid
  real(DP) :: q
  !! q
  real(DP) :: jl (msh)
  !! Output: Spherical Bessel function \(j_l(q*r(i))\)
  !
  ! xseries = convergence radius of the series for small x of j_l(x)
  real(DP) :: x, xl, xseries = 0.05_dp
  integer :: ir, ir0
  integer, external:: semifact
  !
  !  case q=0

  if (abs (q) < eps14) then
     if (l == -1) then
        call upf_error ('sph_bes', 'j_{-1}(0) ?!?', 1)
     elseif (l == 0) then
        jl(:) = 1.d0
     else
        jl(:) = 0.d0
     endif
     return
  end if 

  !  case l=-1

  if (l == - 1) then
     if (abs (q * r (1) ) < eps14) call upf_error ('sph_bes', 'j_{-1}(0) ?!?',1)

     jl (:) = cos (q * r (:) ) / (q * r (:) )

     return

  end if

  ! series expansion for small values of the argument
  ! ir0 is the first grid point for which q*r(ir0) > xseries
  ! notice that for small q it may happen that q*r(msh) < xseries !

  ir0 = msh+1
  do ir = 1, msh
     if ( abs (q * r (ir) ) > xseries ) then
        ir0 = ir
        exit
     end if
  end do

  do ir = 1, ir0 - 1
     x = q * r (ir)
     if ( l == 0 ) then
        xl = 1.0_dp
     else
        xl = x**l
     end if
     jl (ir) = xl/semifact(2*l+1) * &
                ( 1.0_dp - x**2/1.0_dp/2.0_dp/(2.0_dp*l+3) * &
                ( 1.0_dp - x**2/2.0_dp/2.0_dp/(2.0_dp*l+5) * &
                ( 1.0_dp - x**2/3.0_dp/2.0_dp/(2.0_dp*l+7) * &
                ( 1.0_dp - x**2/4.0_dp/2.0_dp/(2.0_dp*l+9) ) ) ) )
  end do

  ! the following shouldn't be needed but do you trust compilers
  ! to do the right thing in this special case ? I don't - PG

  if ( ir0 > msh ) return

  if (l == 0) then

     jl (ir0:) = sin (q * r (ir0:) ) / (q * r (ir0:) )

  elseif (l == 1) then

     jl (ir0:) = (sin (q * r (ir0:) ) / (q * r (ir0:) ) - &
                  cos (q * r (ir0:) ) ) / (q * r (ir0:) )

  elseif (l == 2) then

     jl (ir0:) = ( (3.d0 / (q*r(ir0:)) - (q*r(ir0:)) ) * sin (q*r(ir0:)) - &
                    3.d0 * cos (q*r(ir0:)) ) / (q*r(ir0:))**2

  elseif (l == 3) then

     jl (ir0:) = (sin (q*r(ir0:)) * &
                  (15.d0 / (q*r(ir0:)) - 6.d0 * (q*r(ir0:)) ) + &
                  cos (q*r(ir0:)) * ( (q*r(ir0:))**2 - 15.d0) ) / &
                  (q*r(ir0:)) **3

  elseif (l == 4) then

     jl (ir0:) = (sin (q*r(ir0:)) * &
                  (105.d0 - 45.d0 * (q*r(ir0:))**2 + (q*r(ir0:))**4) + &
                  cos (q*r(ir0:)) * &
                  (10.d0 * (q*r(ir0:))**3 - 105.d0 * (q*r(ir0:))) ) / &
                     (q*r(ir0:))**5
  elseif (l == 5) then

     jl (ir0:) = (-cos(q*r(ir0:)) - &
                  (945.d0*cos(q*r(ir0:))) / (q*r(ir0:)) ** 4 + &
                  (105.d0*cos(q*r(ir0:))) / (q*r(ir0:)) ** 2 + &
                  (945.d0*sin(q*r(ir0:))) / (q*r(ir0:)) ** 5 - &
                  (420.d0*sin(q*r(ir0:))) / (q*r(ir0:)) ** 3 + &
                  ( 15.d0*sin(q*r(ir0:))) / (q*r(ir0:)) ) / (q*r(ir0:))

  elseif (l == 6) then

     jl (ir0:) = ((-10395.d0*cos(q*r(ir0:))) / (q*r(ir0:))**5 + &
                  (  1260.d0*cos(q*r(ir0:))) / (q*r(ir0:))**3 - &
                  (    21.d0*cos(q*r(ir0:))) / (q*r(ir0:))    - &
                             sin(q*r(ir0:))                   + &
                  ( 10395.d0*sin(q*r(ir0:))) / (q*r(ir0:))**6 - &
                  (  4725.d0*sin(q*r(ir0:))) / (q*r(ir0:))**4 + &
                  (   210.d0*sin(q*r(ir0:))) / (q*r(ir0:))**2 ) / (q*r(ir0:))
     
  else

     call upf_error ('sph_bes', 'not implemented', abs(l))

  endif
  !
  return
end subroutine sph_bes

integer function semifact(n)
  ! semifact(n) = n!!
  implicit none
  integer :: n, i

  semifact = 1
  do i = n, 1, -2
     semifact = i*semifact
  end do
  return
end function semifact
!
!----------------------------------------------------------------------------
SUBROUTINE upf_error( calling_routine, message, ierr )
  !----------------------------------------------------------------------------
  !
  ! ... Writes an error message to output (unit "*") if ierr != 0
  ! ... Stops if ierr > 0. Does nothing if ierr = 0 
  !
  IMPLICIT NONE
  !
  CHARACTER(LEN=*), INTENT(IN) :: calling_routine, message
    ! the name of the calling calling_routine
    ! the output message
  INTEGER,          INTENT(IN) :: ierr
  !
  CHARACTER(LEN=6) :: cerr
  INTEGER          :: info
  !
  IF( ierr < 0 ) THEN
     WRITE( UNIT = *, FMT = '(5X,"Message from routine ",A,":")' ) &
             TRIM(calling_routine)
     WRITE( UNIT = *, FMT = '(5X,A)' ) TRIM(message)
     RETURN
  ELSE IF( ierr > 0 ) THEN
     !
     WRITE( cerr, FMT = '(I6)' ) ierr
     WRITE( UNIT = *, FMT = '(/,1X,78("%"))' )
     WRITE( UNIT = *, FMT = '(5X,"Error in routine ",A," (",A,"):")' ) &
          TRIM(calling_routine), TRIM(ADJUSTL(cerr))
     WRITE( UNIT = *, FMT = '(5X,A)' ) TRIM(message)
     WRITE( UNIT = *, FMT = '(1X,78("%"),/)' )
     !
     WRITE( *, '("     stopping ...")' )
     !
     STOP 1
  END IF
  !
END SUBROUTINE upf_error
