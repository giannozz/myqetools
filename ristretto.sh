#!

#/bin/rm -r CPV XSpectra PWCOND VdW PHonon PlotPhon NEB GWW QHA upftools \
#           atomic dev-tools West PP GUI TDDFPT COUPLE

mv PW/src/pwscf.f90 PW/src/pwscf.f90.orig
mv PW/src/h_psi.f90 PW/src/h_psi.f90.orig

cp pwscf_simplified.f90 PW/src/pwscf.f90
cp h_psi_simplified.f90 PW/src/h_psi.f90

# revert with:
# rm PW/src/pwscf.o PW/src/h_psi.o
# mv PW/src/pwscf.f90 pwscf_simplified.f90
# mv PW/src/pwscf.f90.orig PW/src/pwscf.f90
# mv PW/src/h_psi.f90 h_psi_simplified.f90
# mv PW/src/h_psi.f90.orig PW/src/h_psi.f90
