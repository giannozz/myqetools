!
! Copyright (C) 2019 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE e_kin ( )
  !----------------------------------------------------------------------------
  !
  ! ... Calculation of kinetic energy contribution to total energy
  ! ... Written by Paolo Giannozzi between Trieste and Palmanova
  ! ... To be called in electrons_scf, for instance after "sum_band"
  !
  USE kinds,                ONLY : DP
  USE mp,                   ONLY : mp_sum
  USE mp_bands,             ONLY : intra_bgrp_comm
  USE mp_pools,             ONLY : inter_pool_comm
  USE klist,                ONLY : nks, ngk, wk
  USE buffers,              ONLY : get_buffer
  USE control_flags,        ONLY : gamma_only
  USE io_files,             ONLY : iunwfc, nwordwfc
  USE noncollin_module,     ONLY : noncolin
  USE wvfct,                ONLY : g2kin, wg, nbnd, npwx
  USE wavefunctions,        ONLY : evc
  !
  IMPLICIT NONE
  !
  REAL(dp) :: ekin
  INTEGER :: npw, ik, ig, ibnd
  !
  ekin = 0.0_dp
  !
  k_loop: DO ik = 1, nks
     !
     npw = ngk(ik)
     IF ( nks > 1 ) &
          CALL get_buffer ( evc, nwordwfc, iunwfc, ik )
     CALL g2_kin (ik)
     !
!$omp parallel do default(shared), private(ibnd,ig) reduction(+:ekin)
     band_loop: DO ibnd = 1, nbnd
        !
        pw_loop: DO ig = 1, npw
           ekin = ekin + wg(ibnd,ik) * g2kin(ig) * &
              ( REAL(evc(ig,ibnd),KIND=dp)**2 + AIMAG(evc(ig,ibnd))**2 )
        END DO pw_loop
        IF ( noncolin ) THEN
           pw_loop2: DO ig = 1, npw
              ekin = ekin + wg(ibnd,ik) * g2kin(ig) * &
                   ( REAL(evc(ig+npwx,ibnd),KIND=dp)**2 + &
                    AIMAG(evc(ig+npwx,ibnd))**2 )
           END DO pw_loop2
        END IF
        !
     END DO band_loop
!$omp end parallel do
     !
  END DO k_loop
  IF ( gamma_only ) ekin = ekin*2.0_dp
  !
  CALL mp_sum (ekin, intra_bgrp_comm)
  CALL mp_sum (ekin, inter_pool_comm)
  !
END SUBROUTINE e_kin
