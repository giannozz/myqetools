# myqetools

This is my personal repository of QE tools:

- __e_kin.f90__: sample code, calculating kinetic energy in QE
- __ristretto.sh__, __h_psi_simplified.f90__, __pwscf_simplified.f90__:
a simple stripped-down version of QE, replacing "punch", for experiments
- __simple_output.f90__: simplified output file with test, for small systems (e.g. Si)
- __smeartest.f90__: how various smearing types look like - written to test ideas on how to get Fermi energy always right with M-P and M-V smearing
- __test_simpson.f90__: test for integration routines
- __pp_example.f90__: sample code showing how to write a simple QE postprocessing
- __YATL.txt__ : Yet Another TODO List, MaX-2 edition

All files are GPL
