!
program testdom
  use dom
  implicit none
  type(node), pointer :: root_p
  type(nodelist), pointer :: output_p, symm_p
  character(len=10) :: filename ='pwscf.xml'
  !
  root_p => parsefile(filename)
  !
  call destroy ( root_p, 6 )
  !
  root_p => parsefile(filename)
  !
  output_p => getelementsbytagname(root_p,"output")
  if ( associated(output_p) ) then
     symm_p   => getelementsbytagname(output_p%node,"symmetries")
     output_p => getelementsbytagname(symm_p%node,"symmetry")
     print *, 'numero elementi: ', getlength ( output_p )
     root_p => item ( getelementsbytagname(symm_p%node,"symmetry"), 7)
     print *, 'tag=',root_p%tag,', got: ',gettagname(root_p)
  end if
  !
  call destroy ( root_p, 6 )
  !
end program testdom
