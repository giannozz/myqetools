!
! Copyright (C) 2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
PROGRAM pwscf_simplified
  !----------------------------------------------------------------------------
  !
  ! ... Stripped-down version of PWSCF, for testing purposes
  !
  USE environment,       ONLY : environment_start
  USE mp_global,         ONLY : mp_startup
  USE mp_diag,           ONLY : mp_start_diag
  USE mp_world,          ONLY : world_comm
  USE mp_pools,          ONLY : intra_pool_comm
  USE mp_bands,          ONLY : intra_bgrp_comm, inter_bgrp_comm
  USE read_input,        ONLY : read_input_file
  USE command_line_options, ONLY: input_file_, command_line, ndiag_
  USE io_global,        ONLY : stdout, ionode, ionode_id
  USE parameters,       ONLY : ntypx, npk, lmaxx
  USE control_flags,    ONLY : gamma_only, conv_elec, lbands
  !
  IMPLICIT NONE
  INTEGER :: exit_status=0
  !
  ! ... initialization
  !
  CALL mp_startup ( )
  CALL mp_start_diag ( ndiag_, world_comm, intra_bgrp_comm, &
       do_distr_diag_inside_bgrp_ = .true. )
  CALL set_mpi_comm_4_solvers( intra_pool_comm, intra_bgrp_comm, &
       inter_bgrp_comm )
  CALL environment_start ( 'PWSCF' )
  CALL read_input_file ('PW', input_file_ )
  CALL iosys()
  IF ( gamma_only ) WRITE( UNIT = stdout, &
     & FMT = '(/,5X,"gamma-point specific algorithms are used")' )
  CALL setup ()
  CALL init_run()
  !
  ! ... band structure calculation
  !
  CALL c_bands_simplified ( )
  !
  ! ... write band eigenvalues (conv_elec is used in print_ks_energies)
  !
  conv_elec = .true.
  lbands = .true. ! prevents strange output in print_ks_energies
  CALL print_ks_energies ( ) 
  !
  CALL unset_mpi_comm_4_solvers()
  CALL stop_run( exit_status )
  CALL do_stop( exit_status )
  !
  STOP
  !
9010 FORMAT( /,5X,'Current dimensions of program PWSCF are:', &
           & /,5X,'Max number of different atomic species (ntypx) = ',I2,&
           & /,5X,'Max number of k-points (npk) = ',I6,&
           & /,5X,'Max angular momentum in pseudopotentials (lmaxx) = ',i2)
  !
END PROGRAM pwscf_simplified
!
SUBROUTINE c_bands_simplified( )
  !----------------------------------------------------------------------------
  !
  ! ... Driver routine for Hamiltonian diagonalization routines
  ! ... specialized to non-self-consistent calculations (no electric field)
  !
  USE kinds,                ONLY : DP
  USE io_global,            ONLY : stdout
  USE klist,                ONLY : nkstot, nks, xk, igk_k, ngk
  USE uspp,                 ONLY : vkb, nkb
  USE gvect,                ONLY : g
  USE wvfct,                ONLY : et, nbnd, npwx, current_k
  USE control_flags,        ONLY : ethr
  USE lsda_mod,             ONLY : current_spin, lsda, isk
  USE mp_pools,             ONLY : inter_pool_comm
  USE mp,                   ONLY : mp_sum
  !
  IMPLICIT NONE
  !
  REAL(DP) :: avg_iter
  ! average number of H*psi products
  INTEGER :: ik, npw
  ! ik : counter on k points
  !
  REAL(DP), EXTERNAL :: get_clock
  !
  CALL start_clock( 'c_bands' )
  !
  avg_iter = 0.D0
  WRITE( stdout, '(5X,"Davidson diagonalization with overlap")' )
  !
  k_loop: DO ik = 1, nks
     !
     ! ... For each k, compute all quantities needed by the Hamiltonian
     !
     current_k = ik
     IF ( lsda ) current_spin = isk(ik)
     npw = ngk(ik)
     IF ( nkb > 0 ) CALL init_us_2( npw, igk_k(1,ik), xk(1,ik), vkb )
     call g2_kin( ik )
     !
     ! ... Compute starting wavefunctions
     !
     CALL init_wfc ( ik )
     !
     ! ... Diagonalize the Hamiltonian
     !
     call diag_bands_simplified ( ik, avg_iter )
     !
  END DO k_loop
  !
  CALL mp_sum( avg_iter, inter_pool_comm )
  avg_iter = avg_iter / nkstot
  WRITE( stdout, '(/,5X,"ethr = ",1PE9.2,",  avg # of iterations =",0PF5.1)' ) &
       ethr, avg_iter
  !
  CALL stop_clock( 'c_bands' )
  !
  RETURN
  !
END SUBROUTINE c_bands_simplified
!
!----------------------------------------------------------------------------
SUBROUTINE diag_bands_simplified ( ik, avg_iter )
  !----------------------------------------------------------------------------
  !
  ! ... Diagonalizes the Hamiltonian at k-point ik, returning in avg_iter
  ! ... the numbero of performed iterations in iterative diagonalization
  !
  USE kinds,                ONLY : DP
  USE buffers,              ONLY : get_buffer
  USE io_global,            ONLY : stdout
  USE io_files,             ONLY : nwordwfc
  USE uspp,                 ONLY : vkb, nkb, okvan
  USE gvect,                ONLY : gstart
  USE wvfct,                ONLY : g2kin, nbndx, et, nbnd, npwx, &
       current_k, btype
  USE control_flags,        ONLY : ethr, gamma_only, use_para_diag
  USE noncollin_module,     ONLY : noncolin, npol
  USE wavefunctions,        ONLY : evc
  USE g_psi_mod,            ONLY : h_diag, s_diag
  USE scf,                  ONLY : v_of_0
  USE becmod,               ONLY : bec_type, becp, calbec, &
                                   allocate_bec_type, deallocate_bec_type
  USE klist,                ONLY : ngk
  USE mp_bands,             ONLY : nproc_bgrp, intra_bgrp_comm
  USE mp,                   ONLY : mp_sum
  !
  IMPLICIT NONE
  !
  INTEGER, INTENT(IN)  :: ik
  REAL (KIND=DP), INTENT(INOUT) :: avg_iter
  !
  INTEGER :: dav_iter, notconv
  ! dav_iter: number of iterations in iterative diagonalization
  INTEGER :: ierr, ipol, npw
  LOGICAL :: lrot = .true.
! Davidson diagonalization uses these external routines on groups of nvec bands
  external h_psi, s_psi, g_psi
  !
  npw = ngk(ik)
  ALLOCATE( h_diag( npwx, npol ), STAT=ierr )
  IF( ierr /= 0 ) &
     CALL errore( ' diag_bands ', ' cannot allocate h_diag ', ABS(ierr) )
  !
  ALLOCATE( s_diag( npwx, npol ), STAT=ierr )
  IF( ierr /= 0 ) &
     CALL errore( ' diag_bands ', ' cannot allocate s_diag ', ABS(ierr) )
  !
  ! ... allocate space for <beta_i|psi_j> - used in h_psi and s_psi
  !
  CALL allocate_bec_type ( nkb, nbnd, becp, intra_bgrp_comm )
  !
  DO ipol = 1, npol
     h_diag(1:npw, ipol) = g2kin(1:npw) + v_of_0
  END DO
  CALL usnldiag( npw, h_diag, s_diag )
  !
  IF ( gamma_only ) THEN
     !
     ! ... Real Hamiltonian
     !
     IF ( use_para_diag ) then
        !
        CALL pregterg( h_psi, s_psi, okvan, g_psi, &
             npw, npwx, nbnd, nbndx, evc, ethr, &
             et(1,ik), btype(1,ik), notconv, lrot, dav_iter )
        !
     ELSE
        !
        CALL regterg ( h_psi, s_psi, okvan, g_psi, &
             npw, npwx, nbnd, nbndx, evc, ethr, &
             et(1,ik), btype(1,ik), notconv, lrot, dav_iter )
        !
     END IF
     !
  ELSE
     !
     ! ... Complex Hamiltonian
     !
     IF ( use_para_diag ) then
        !
        CALL pcegterg( h_psi, s_psi, okvan, g_psi, &
             npw, npwx, nbnd, nbndx, npol, evc, ethr, &
             et(1,ik), btype(1,ik), notconv, lrot, dav_iter )
        !
     ELSE
        !
        CALL cegterg ( h_psi, s_psi, okvan, g_psi, &
             npw, npwx, nbnd, nbndx, npol, evc, ethr, &
             et(1,ik), btype(1,ik), notconv, lrot, dav_iter )
        !
     END IF
     !
  END IF
  !
  avg_iter = avg_iter + dav_iter
  !
  ! ... deallocate work space
  !
  CALL deallocate_bec_type ( becp )
  DEALLOCATE( s_diag )
  DEALLOCATE( h_diag )
  !
  IF ( notconv > 0 ) THEN
     WRITE( stdout, '(5X,"c_bands: ",I2, &
               &   " eigenvalues not converged")' ) notconv
  END IF
  !
  RETURN
  !
END SUBROUTINE diag_bands_simplified
