!
program testdom
  !
  use dom
  use wxml
  implicit none
  ! wxml
  type(xmlf_t) :: xf
  integer :: ounit, ierr
  ! dom
  type(node), pointer :: root_p
  ! data
  real(8) :: rnum=1.23_8
  real(8), dimension(3) :: rvec=[1.23_8, 3.12_8, 2.31_8]
  real(8), dimension(3,3)::rmat
  integer :: inum=42
  integer, dimension(3) :: ivec=[43, 44, 45]
  logical :: lnum=.true.
  logical, dimension(3) :: lvec=[.false., .true., .false.]
  !
  !!! write with WXML
  !
  call xml_OpenFile(FILENAME = 'test.xml', XF = xf, UNIT = ounit, &
              PRETTY_PRINT = .TRUE., REPLACE  = .TRUE., NAMESPACE = .TRUE., &
              IOSTAT = ierr )
  call xml_DeclareNamespace (XF=xf, PREFIX = "xsi", nsURI ="http://www.w3.org/2001/XMLSchema-instance")
  call xml_DeclareNamespace (XF=xf, PREFIX = "qes", nsURI ="http://www.quantum-espresso.org/ns/qes/qes-1.0")
  call xml_NewElement (XF=xf, NAME = "qes:espresso")
  call xml_addAttribute(XF=xf, NAME = "xsi:schemaLocation", &
       VALUE = "http://www.quantum-espresso.org/ns/qes/qes-1.0 "//&
       "http://www.quantum-espresso.org/ns/qes/qes_220603.xsd" )
  call xml_addAttribute(XF=xf, NAME="Units", VALUE="Hartree atomic units")
  call xml_addComment(XF = xf, COMMENT = "This is a comment" )
  !
  call xml_NewElement (xf, NAME = "char")
  call xml_addCharacters(xf, "a character string")
  call xml_EndElement(xf, "char")
  !
  call xml_NewElement (xf, NAME = "real_data")
  !
  call xml_NewElement (xf, NAME = "real_num")
  call xml_addAttribute(xf, NAME="desc", VALUE=rnum)
  call xml_addCharacters(xf, rnum)
  call xml_EndElement(xf, "real_num")
  !
  call xml_NewElement (xf, NAME = "real_vec")
  call xml_addAttribute(xf, NAME="desc", VALUE="a real vector")
  call xml_addCharacters(xf, rvec)
  call xml_EndElement(xf, "real_vec")
  !
  rmat(1:3,1) = [1.23, 3.12, 2.31]
  rmat(1:3,2) = [4.56, 6.45, 5.64]
  rmat(1:3,3) = [7.89, 9.78, 8.97 ]
  call xml_NewElement (xf, NAME = "real_mat")
  call xml_addAttribute(xf, NAME="desc", VALUE="a real matrix")
  call xml_addCharacters(xf, rmat)
  call xml_EndElement(xf, "real_mat")
  !
  call xml_EndElement(xf, "real_data")
  !
  call xml_NewElement (xf, NAME = "int_data")
  !
  call xml_NewElement (xf, NAME = "integer")
  call xml_addAttribute(xf, NAME="desc", VALUE=inum)
  call xml_addCharacters(xf, inum)
  call xml_EndElement(xf, "integer")
  !
  call xml_NewElement (xf, NAME = "int_vec")
  call xml_addAttribute(xf, NAME="desc", VALUE=ivec)
  call xml_addCharacters(xf, ivec)
  call xml_EndElement(xf, "int_vec")
  !
  call xml_EndElement(xf, "int_data")
  !
  call xml_NewElement (xf, NAME = "log_data")
  !
  call xml_NewElement (xf, NAME = "logical")
  call xml_addAttribute(xf, NAME="desc", VALUE=lnum)
  call xml_addCharacters(xf, lnum)
  call xml_EndElement(xf, "logical")
  !
  call xml_NewElement (xf, NAME = "log_vec")
  call xml_addAttribute(xf, NAME="desc", VALUE=.not.lnum)
  call xml_addCharacters(xf, lvec)
  call xml_EndElement(xf, "log_vec")
  !
  call xml_EndElement(xf, "log_data")
  !
  call xml_EndElement(xf, "qes:espresso")
  call xml_Close(xf) 
  !
  !!! now read with DOM
  !
  root_p => parsefile('test.xml')
  call destroy ( root_p, 6 )
  !
end program testdom
